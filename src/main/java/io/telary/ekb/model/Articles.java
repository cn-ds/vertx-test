package io.telary.ekb.model;

import java.util.ArrayList;

public interface Articles extends Iterable<Article> {
	public Article add(String title, String content, String path, String type);
	public ArrayList<Article> find(String query);
}

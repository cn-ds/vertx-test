package io.telary.ekb.model;

public interface Article {
	public String title();
	public String content();
	public String path();
	public String type();
}

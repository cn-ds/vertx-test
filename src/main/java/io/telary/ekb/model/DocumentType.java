package io.telary.ekb.model;

public enum DocumentType {
	DOCX,
	DOC,
	MD,
	PPT,
	PPTX,
	WEB	
}

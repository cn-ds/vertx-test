package io.telary.ekb.main;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import io.telary.ekb.endpoints.ArticleEndpoint;
import io.telary.ekb.endpoints.ArticlesEndpoint;
import io.telary.ekb.endpoints.DefaultEndpoint;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class RestApi {

	public static void main(String[] args) {
		VertxOptions vxOptions = new VertxOptions()
				.setBlockedThreadCheckInterval(200_000_000_000L);
		Vertx vertx = Vertx.vertx(vxOptions);
		
		HttpServer server = vertx.createHttpServer();

		Router router = Router.router(vertx);
		
		RestHighLevelClient client = new RestHighLevelClient(
		        RestClient.builder(
		                new HttpHost("localhost", 9200, "http")));
		
		var articlesEndpoint = new ArticlesEndpoint(client);
		var articleEndpoint = new ArticleEndpoint(client);
		var fallbackEndpoint = new DefaultEndpoint(client);
		
		router.route().handler(BodyHandler.create());

		router.get("/articles").handler(articlesEndpoint::getHandle);
		router.get("/article/{id}").handler(articleEndpoint::getHandle);
		router.post("/article").handler(articleEndpoint::postHandle);
		router.route().handler(fallbackEndpoint::getHandle);

		server.requestHandler(router).listen(8080);
	}
}

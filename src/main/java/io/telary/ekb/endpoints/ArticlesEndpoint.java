package io.telary.ekb.endpoints;

import org.elasticsearch.client.RestHighLevelClient;

import io.telary.ekb.db.EsArticles;
import io.telary.ekb.endpoint.JsonEndpoint;
import io.telary.ekb.model.Article;
import io.telary.ekb.model.Articles;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public final class ArticlesEndpoint extends JsonEndpoint {

	public ArticlesEndpoint(RestHighLevelClient client) {
		super(client);
	}

	@Override
	protected String prepareGetResponse(RoutingContext routingContext) {
		String query = routingContext.request().getParam("query");
		Articles articles = new EsArticles(this.client);
		JsonArray jsonArray = new JsonArray();
		for (Article article : articles.find(query)) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.put("title", article.title());
			jsonObject.put("content", article.content());
			jsonObject.put("path", article.path());
			jsonObject.put("type", article.type());
			jsonArray.add(jsonObject);
		}
		
		return jsonArray.toString();
	}

	@Override
	protected String preparePostResponse(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String preparePutResponse(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String prepareDeleteResponse(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		return null;
	}

}

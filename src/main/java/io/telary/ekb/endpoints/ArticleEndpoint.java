package io.telary.ekb.endpoints;

import org.elasticsearch.client.RestHighLevelClient;

import io.telary.ekb.db.EsArticles;
import io.telary.ekb.endpoint.JsonEndpoint;
import io.telary.ekb.model.Articles;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class ArticleEndpoint extends JsonEndpoint {

	public ArticleEndpoint(RestHighLevelClient client) {
		super(client);
	}

	@Override
	protected String prepareGetResponse(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String preparePostResponse(RoutingContext routingContext) {
		JsonObject jsonArticle = routingContext.getBodyAsJson();
		Articles articles = new EsArticles(this.client);
		articles.add(
				jsonArticle.getString("title"), jsonArticle.getString("content"),
				jsonArticle.getString("path"), jsonArticle.getString("type"));
		return "OK";
	}

	@Override
	protected String preparePutResponse(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String prepareDeleteResponse(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		return null;
	}



}

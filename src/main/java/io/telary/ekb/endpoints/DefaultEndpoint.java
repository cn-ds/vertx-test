package io.telary.ekb.endpoints;

import org.elasticsearch.client.RestHighLevelClient;

import io.telary.ekb.endpoint.AbstractEndpoint;
import io.vertx.ext.web.RoutingContext;

public final class DefaultEndpoint extends AbstractEndpoint {

	public DefaultEndpoint(RestHighLevelClient client) {
		super(client);
	}

	@Override
	protected String prepareGetResponse(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String preparePostResponse(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String preparePutResponse(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String prepareDeleteResponse(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		return null;
	}

}

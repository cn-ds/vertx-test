package io.telary.ekb.endpoint;

import io.vertx.ext.web.RoutingContext;

public interface Endpoint {
	public void getHandle(RoutingContext routingContext);
	public void postHandle(RoutingContext routingContext);
	public void putHandle(RoutingContext routingContext);
	public void deleteHandle(RoutingContext routingContext);
}

package io.telary.ekb.endpoint;

import io.vertx.ext.web.RoutingContext;

@FunctionalInterface
public interface HandleAction {
	public String run(RoutingContext routingContext);
}

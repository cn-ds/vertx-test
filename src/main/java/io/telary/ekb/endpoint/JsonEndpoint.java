package io.telary.ekb.endpoint;

import org.elasticsearch.client.RestHighLevelClient;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;

public abstract class JsonEndpoint extends AbstractEndpoint {
	
	public JsonEndpoint(RestHighLevelClient client) {
		super(client);
	}

	@Override
	public HttpServerResponse prepareResponse(RoutingContext routingContext) {
		  HttpServerResponse response = routingContext.response();
		  response.putHeader("content-type", "text/json");
		  return response;
	}
}

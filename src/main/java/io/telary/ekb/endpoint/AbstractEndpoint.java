package io.telary.ekb.endpoint;

import org.elasticsearch.client.RestHighLevelClient;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;

public abstract class AbstractEndpoint implements Endpoint {
	
	protected RestHighLevelClient client;
	
	public AbstractEndpoint(final RestHighLevelClient client) {
		this.client = client;
	}

	public void getHandle(RoutingContext routingContext) {
		callHandler(routingContext, this::prepareGetResponse);
	}
	
	public void postHandle(RoutingContext routingContext) {
		callHandler(routingContext, this::preparePostResponse);
	}
	
	public void putHandle(RoutingContext routingContext) {
		callHandler(routingContext, this::preparePutResponse);
	}
	
	public void deleteHandle(RoutingContext routingContext) {
		callHandler(routingContext, this::prepareDeleteResponse);
	}
	
	private void callHandler(RoutingContext routingContext, HandleAction action) {
		try {
			prepareResponse(routingContext).end(action.run(routingContext));
		} catch (UnsupportedOperationException e) {
			displayError(routingContext, e);
		}
	}
	
	private void displayError(RoutingContext routingContext, Exception e) {
		HttpServerResponse response = prepareResponse(routingContext);
		response.setStatusCode(500);
		response.setStatusMessage(e.getMessage());
		response.end();
	}
	
	protected HttpServerResponse prepareResponse(RoutingContext routingContext) {
		  HttpServerResponse response = routingContext.response();
		  response.putHeader("content-type", "text/plain");
		  return response;
	}
	
	protected abstract String prepareGetResponse(RoutingContext routingContext);
	protected abstract String preparePostResponse(RoutingContext routingContext);
	protected abstract String preparePutResponse(RoutingContext routingContext);
	protected abstract String prepareDeleteResponse(RoutingContext routingContext);
}

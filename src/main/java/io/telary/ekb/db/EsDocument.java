package io.telary.ekb.db;

import org.elasticsearch.client.RestHighLevelClient;

public interface EsDocument {

	public void save(RestHighLevelClient client);
}

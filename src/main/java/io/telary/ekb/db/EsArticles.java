package io.telary.ekb.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import io.telary.ekb.model.Article;
import io.telary.ekb.model.Articles;

public class EsArticles implements Articles {
	
	private final RestHighLevelClient client;

	public EsArticles(RestHighLevelClient client) {
		this.client = client;
	}
	
	@Override
	public ArrayList<Article> find(String query) {
		SearchRequest searchRequest = new SearchRequest("articles");
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder(); 
		searchSourceBuilder.query(QueryBuilders.multiMatchQuery(query, 
				"title", "content", "path", "type"));
		searchSourceBuilder.size(10);
		searchRequest.source(searchSourceBuilder);
		ArrayList<Article> articles = new ArrayList<>();
		try {
			SearchResponse response = this.client.search(searchRequest, RequestOptions.DEFAULT);
			response.getHits().forEach(hit -> {
				articles.add(new EsArticle(this.client, hit));
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return articles;
	}
	
	@Override
	public Iterator<Article> iterator() {
		SearchRequest searchRequest = new SearchRequest("articles");
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder(); 
		searchSourceBuilder.query(QueryBuilders.matchAllQuery());
		searchSourceBuilder.size(10);
		searchRequest.source(searchSourceBuilder);
		ArrayList<Article> articles = new ArrayList<>();
		try {
			SearchResponse response = this.client.search(searchRequest, RequestOptions.DEFAULT);
			response.getHits().forEach(hit -> {
				articles.add(new EsArticle(this.client, hit));
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return articles.iterator();
	}

	@Override
	public Article add(String title, String content, String path, String type) {
		IndexRequest request = new IndexRequest("articles");
		request.source("title", title, "content", content, "path", path, "type", type);
		try {
			this.client.index(request, RequestOptions.DEFAULT);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new EsArticle(this.client, title, content, path, type);
	}

}

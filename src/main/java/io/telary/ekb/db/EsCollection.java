package io.telary.ekb.db;

public interface EsCollection<T> {

	public T fetch();
}

package io.telary.ekb.db;

import java.util.Map;

import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.SearchHit;

import io.telary.ekb.model.Article;

public class EsArticle implements Article {
	
	@SuppressWarnings("unused")
	private final RestHighLevelClient client;
	private final String title;
	private final String content;
	private final String path;
	private final String type;
	
	public EsArticle(RestHighLevelClient client, String title, String content, String path, String type) {
		this.client = client;
		this.title = title;
		this.content = content;
		this.path = path;
		this.type = type;
	}
	
	public EsArticle(RestHighLevelClient client, SearchHit hit) {
		Map<String, Object> resultMap = hit.getSourceAsMap();
		this.client = client;
		this.title = (String) resultMap.get("title");
		this.content = (String) resultMap.get("content");
		this.path = (String) resultMap.get("path");
		this.type = (String) resultMap.get("type");
	}

	@Override
	public String title() {
		return this.title;
	}

	@Override
	public String content() {
		return this.content;
	}

	@Override
	public String path() {
		return this.path;
	}

	@Override
	public String type() {
		return this.type;
	}
}

# Elasticsearch configuration files

## Création du mapping

````
PUT telary-index-v0.1.0
{
  ... # contenu de mapping.json
}
````

## Insertion de quelques données

````
POST telary-index-v0.1.0/_doc
{
  ... # une ligne de data.json
}
````